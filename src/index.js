import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
function Square(props) {
  return (
    <button className={props.className} onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i, className) {
    return (
      <Square
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
        className={className}
      />
    );
  }

  render() {
    let squares = [];
    for (let i = 0; i < 3; i++) {
      squares.push(<div className="board-row"></div>);
      for (let j = 0; j < 3; j++) {
        const position = i * 3 + j;
        let classes = "square";
        if (
          Array.isArray(this.props.winner) &&
          this.props.winner.includes(position)
        ) {
          classes += " winning_tile";
        }
        squares.push(this.renderSquare(position, classes));
      }
    }
    return <div>{squares}</div>;
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      ascendingMoves: true,
      stepNumber: 0,
      xIsNext: true
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({
      history: history.concat([
        {
          squares: squares
        }
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0
    });
  }

  reverseMoveOrder() {
    this.setState({
      ascendingMoves: !this.state.ascendingMoves
    });
  }

  render() {
    let history = this.state.history;

    const current = history[this.state.stepNumber];
    let prev = history[0].squares;

    const moves = history.map((step, move) => {
      let desc;
      let position;
      if (move) {
        for (let i = 0; i < prev.length; i++) {
          if (prev[i] !== step.squares[i]) {
            position = i;
            break;
          }
        }
        const col = (position % 3) + 1;
        const row = Math.floor(position / 3 + 1);
        prev = step.squares;
        desc = "(" + col + "," + row + ") Go to move #" + move;
      } else {
        desc = "Go to game start";
      }

      let boldDesc;
      if (move == this.state.stepNumber) {
        boldDesc = desc;
        desc = "";
      } else {
        boldDesc = "";
      }

      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>
            <b>{boldDesc}</b>
            {desc}
          </button>
        </li>
      );
    });

    if (!this.state.ascendingMoves) {
      moves.reverse();
    }

    const winner = calculateWinner(current.squares);
    let status;
    if (winner) {
      if (winner == "draw") {
        status = "Game is a draw!";
      } else {
        status = "Winner: " + current.squares[winner[0]];
      }
    } else {
      status = "Next player: " + (this.state.xIsNext ? "X" : "O");
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={i => this.handleClick(i)}
            winner={winner}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>
            <button onClick={() => this.reverseMoveOrder()}>
              Reverse Move Order
            </button>
            {moves}
          </ol>
        </div>
      </div>
    );
  }
}

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return lines[i];
    }
  }
  if (!squares.includes(null)) {
    return "draw";
  }
  return null;
}

// ========================================

ReactDOM.render(<Game />, document.getElementById("root"));
